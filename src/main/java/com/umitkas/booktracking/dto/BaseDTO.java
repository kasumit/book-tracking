package com.umitkas.booktracking.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Comparator;
import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseDTO{

    private String id;

    private Date createdAt;

    private Date updatedAt;


}
