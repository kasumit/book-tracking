package com.umitkas.booktracking.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponse {

    private boolean success;

    private String record;

    @CreatedDate
    private long timestamp;


    public ApiResponse(boolean success, String record) {
        this.success = success;
        this.record = record;
    }
}
