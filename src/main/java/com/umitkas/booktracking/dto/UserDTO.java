package com.umitkas.booktracking.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnore;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO {

    private String username;

    @JsonIgnore
    private String password;
}
