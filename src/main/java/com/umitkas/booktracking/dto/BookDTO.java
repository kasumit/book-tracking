package com.umitkas.booktracking.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookDTO extends BaseDTO{

    private String name;

    private String author;

    private List<BookCollectionDTO> collections;

}
