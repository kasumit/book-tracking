package com.umitkas.booktracking.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookCollectionDTO extends BaseDTO{

    private String name;

    private List<BookDTO> books;

    private UserDTO owner;

    private boolean publiclyShared;
}
