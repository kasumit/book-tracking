package com.umitkas.booktracking;

import com.umitkas.booktracking.customExceptions.ServiceException;
import com.umitkas.booktracking.customExceptions.UsernameAlreadyExistException;
import com.umitkas.booktracking.service.UserService;
import com.umitkas.booktracking.dto.UserDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@EnableCaching
public class App {

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

	@Bean
	public CacheManager getCacheManager(){
		return new ConcurrentMapCacheManager("books");
	}

	@Bean
	public BCryptPasswordEncoder getPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}




	@Autowired
	private void mockData(final UserService userService) throws ServiceException, UsernameAlreadyExistException {

		try {

			UserDTO userDTO = new UserDTO();
			userDTO.setUsername("mock.user");
			userDTO.setPassword("mock.password");
			userService.register(userDTO);


			userDTO = new UserDTO();
			userDTO.setUsername("mock.user2");
			userDTO.setPassword("mock.password");
			userService.register(userDTO);
		}
		catch (Exception e){

        }


	}

}
