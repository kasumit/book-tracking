package com.umitkas.booktracking.repository;

import com.umitkas.booktracking.domain.Book;
import com.umitkas.booktracking.domain.BookCollection;
import com.umitkas.booktracking.domain.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface BookCollectionRepository extends JpaRepository<BookCollection, String>{

    List<BookCollection> findAllByPubliclySharedAndOwnerNotOrderByUpdatedAtDesc(boolean shared, User owner, Pageable pageable);

    List<BookCollection> findAllByPubliclySharedAndOwnerOrderByUpdatedAtDesc(boolean shared, User owner, Pageable pageable);

    List<BookCollection> findAllByOwnerOrderByUpdatedAtDesc(User owner, Pageable pageable);

    BookCollection findById(String id);

    void deleteById(String id);

    boolean existsByIdAndOwner(String id, User owner);


}
