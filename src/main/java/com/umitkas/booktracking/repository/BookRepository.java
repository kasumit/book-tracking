package com.umitkas.booktracking.repository;

import com.umitkas.booktracking.domain.Book;
import com.umitkas.booktracking.domain.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
@Repository
@Transactional
public interface BookRepository extends JpaRepository<Book, String>{

    List<Book> findAllByOwnerOrderByUpdatedAtDesc(User owner, Pageable pageable);

    Book findById(String id);

    void deleteById(String id);

    boolean existsByIdAndOwner(String id, User owner);

    List<Book> findAllByIdInAndOwner(List<String> bookIds, User owner);

}
