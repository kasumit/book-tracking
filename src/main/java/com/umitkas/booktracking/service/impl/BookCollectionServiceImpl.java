package com.umitkas.booktracking.service.impl;

import com.umitkas.booktracking.customExceptions.DataAlreadyExistException;
import com.umitkas.booktracking.customExceptions.DataNotFoundException;
import com.umitkas.booktracking.customExceptions.NotAuthenticatedFunctionalityException;
import com.umitkas.booktracking.customExceptions.ServiceException;
import com.umitkas.booktracking.domain.Book;
import com.umitkas.booktracking.domain.BookCollection;
import com.umitkas.booktracking.domain.User;
import com.umitkas.booktracking.dto.ApiResponse;
import com.umitkas.booktracking.dto.BookCollectionDTO;
import com.umitkas.booktracking.dto.BookDTO;
import com.umitkas.booktracking.dto.UserDTO;
import com.umitkas.booktracking.repository.BookCollectionRepository;
import com.umitkas.booktracking.service.BookCollectionService;
import com.umitkas.booktracking.service.BookService;
import com.umitkas.booktracking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookCollectionServiceImpl implements BookCollectionService{


    @Autowired
    private BookCollectionRepository bookCollectionRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private BookService bookService;

    /**
     * returns the book collection list of authenticated user,
     * returns 5 object for each page
     *
     * @author umit.kas
     * @param page
     * @return List<BookCollectionDTO>
     *
     */

    @Override
    public List<BookCollectionDTO> getAuthenticatedUserBookCollections(int page) throws DataNotFoundException {
        User authUser = userService.getAuthenticatedUserEntity();

        List<BookCollection> entities = bookCollectionRepository.findAllByOwnerOrderByUpdatedAtDesc(authUser, new PageRequest(page, 5));

        if (entities == null){
            throw new DataNotFoundException("No such a book collection is found for authenticated user");

        }
        return entitiesToDTOs(entities);
    }


    /**
     * returns the publicly shared book collection list of other users,
     * returns 5 object for each page
     *
     * @author umit.kas
     * @param page
     * @return List<BookCollectionDTO>
     *
     */

    @Override
    public List<BookCollectionDTO> getPubliclySharedBookCollectionsByOtherUsers(int page) throws DataNotFoundException {
        User authUser = userService.getAuthenticatedUserEntity();
        List<BookCollection> entities = bookCollectionRepository.findAllByPubliclySharedAndOwnerNotOrderByUpdatedAtDesc(true, authUser, new PageRequest(page, 5));

        if (entities == null){
            throw new DataNotFoundException("No such a book collection is found for authenticated user");
        }
        return entitiesToDTOs(entities);

    }

    /**
     * returns the book collection list of authenticated user by publicity,
     * returns 5 object for each page
     *
     * @author umit.kas
     * @param shared
     * @param page
     * @return List<BookCollectionDTO>
     *
     */

    @Override
    public List<BookCollectionDTO> getAuthenticatedUserBookCollectionsByPubliclyShared(boolean shared, int page) throws DataNotFoundException {

        User authUser = userService.getAuthenticatedUserEntity();

        List<BookCollection> entities = bookCollectionRepository.findAllByPubliclySharedAndOwnerOrderByUpdatedAtDesc(shared, authUser, new PageRequest(page, 5));

        if (entities == null){
            String text;
            if (shared)
                text = "public";
            else
                text = "private";

            throw new DataNotFoundException(String.format("No such a %s book collection is found for authenticated user", text));
        }
        return entitiesToDTOs(entities);

    }

    /**
     * returns the book collection list,
     * converts the entities to dtos
     *
     * @author umit.kas
     * @param entities
     * @return List<BookCollectionDTO>
     *
     */

    private List<BookCollectionDTO> entitiesToDTOs(List<BookCollection> entities){
        // model mapper can short the code, but never used it before.
        // TODO: 14.02.2018 @umit.kas fix with model mapper

        // converts book collections entity to dto
        List<BookCollectionDTO> bookCollectionDTOs = entities
                .stream()
                .map(entity -> {
                    BookCollectionDTO dto = new BookCollectionDTO();

                    dto.setId(entity.getId());
                    dto.setName(entity.getName());
                    dto.setPubliclyShared(entity.isPubliclyShared());

                    // add user info
                    UserDTO userDTO = new UserDTO();
                    userDTO.setUsername(entity.getOwner().getUsername());
                    dto.setOwner(userDTO);

                    // --

                    // convert books entity to dto
                    if (entity.getBooks() != null){
                        List<BookDTO> bookDTOS = entity.getBooks()
                                .stream()
                                .map(bookEntity -> {
                                    BookDTO bookDTO = new BookDTO();
                                    bookDTO.setId(bookEntity.getId());
                                    bookDTO.setName(bookEntity.getName());
                                    bookDTO.setAuthor(bookEntity.getAuthor());
                                    return bookDTO;
                                })
                                .collect(Collectors.toList());
                        // add book list to collection
                        dto.setBooks(bookDTOS);
                    }
                    // --

                    return dto;
                })
                .collect(Collectors.toList());

        return bookCollectionDTOs;

    }

    /**
     * saves book collection for authenticated user,
     * returns ApiResponse object that contains is success, and if it is record, which is the entity id
     *
     * @author umit.kas
     * @param dto
     * @return ApiResponse
     *
     */
    @Override
    public ApiResponse save(BookCollectionDTO dto) throws ServiceException {

        User authUser = userService.getAuthenticatedUserEntity();

        BookCollection entity = new BookCollection();

        entity.setName(dto.getName());
        entity.setOwner(authUser);
        entity.setPubliclyShared(dto.isPubliclyShared());

        entity = bookCollectionRepository.save(entity);

        if (entity == null || entity.getId().isEmpty()){
            throw new ServiceException("No such a book collection is saved");

        }

        return new ApiResponse(true, entity.getId());
    }

    /**
     * updates book collection for authenticated user,
     * returns ApiResponse object that contains is success, and if it is record, which is the entity id
     *
     * @author umit.kas
     * @param dto
     * @return ApiResponse
     *
     */
    @Override
    public ApiResponse update(BookCollectionDTO dto) throws NotAuthenticatedFunctionalityException, ServiceException {

        User authUser = userService.getAuthenticatedUserEntity();

        String id = dto.getId();

        if (!bookCollectionRepository.existsByIdAndOwner(id, authUser)){
            throw new NotAuthenticatedFunctionalityException(String.format("Authenticated user has no permission to update book collection with id: %s", id));
        }

        BookCollection entity = bookCollectionRepository.findById(id);

        entity.setName(dto.getName());
        entity.setPubliclyShared(dto.isPubliclyShared());

        entity = bookCollectionRepository.save(entity);


        if (entity == null || entity.getId().isEmpty()){
            throw new ServiceException("No such a book collection is updated");
        }

        return new ApiResponse(true, entity.getId());
    }

    /**
     * add book to book collection for authenticated user,
     * finds books by bookIds parameter from bookService, get as entity,
     * then add in book list of book collection
     * then saves
     * returns ApiResponse object that contains is success, and if it is record, which is the entity id
     *
     * @author umit.kas
     * @param collectionId
     * @param bookIds
     * @return ApiResponse
     *
     */
    @Override
    public ApiResponse addBooks(String collectionId, List<String> bookIds) throws NotAuthenticatedFunctionalityException, DataNotFoundException, ServiceException, DataAlreadyExistException {
        User authUser = userService.getAuthenticatedUserEntity();


        if (!bookCollectionRepository.existsByIdAndOwner(collectionId, authUser)){
            throw new NotAuthenticatedFunctionalityException(String.format("Authenticated user has no permission to update book collection with id: %s", collectionId));
        }

        BookCollection entity = bookCollectionRepository.findById(collectionId);

        List<Book> bookEntities = bookService.findAllById(bookIds);

        final List<Book> collectionBookList = entity.getBooks();

        List<Book> notInCollectionList = bookEntities
                .stream()
                .filter(book -> !collectionBookList.contains(book))
                .collect(Collectors.toList());


        if (notInCollectionList.size() > 0) {
            entity.getBooks().addAll(notInCollectionList);

            entity = bookCollectionRepository.save(entity);

            if (entity == null || entity.getId().isEmpty()) {
                throw new ServiceException(String.format("No such a book is added to collection by id: %s ", collectionId));
            }
            return new ApiResponse(true, entity.getId());
        }
        throw new DataAlreadyExistException(String.format("No such a book is added to collection by id: %s, reason can be the book/books are already added to the collection ", collectionId));
    }


    /**
     * delete book from book collection for authenticated user,
     * finds books by bookIds parameter from bookService, get as entity,
     * then delete in book list of book collection
     * then saves
     * returns ApiResponse object that contains is success, and if it is record, which is the entity id
     *
     * @author umit.kas
     * @param collectionId
     * @param bookIds
     * @return ApiResponse
     *
     */
    @Override
    public ApiResponse deleteBooks(String collectionId, List<String> bookIds) throws NotAuthenticatedFunctionalityException, DataNotFoundException, ServiceException {
        User authUser = userService.getAuthenticatedUserEntity();


        if (!bookCollectionRepository.existsByIdAndOwner(collectionId, authUser)){
            throw new NotAuthenticatedFunctionalityException(String.format("Authenticated user has no permission to update book collection with id: %s", collectionId));
        }

        BookCollection entity = bookCollectionRepository.findById(collectionId);

        List<Book> bookEntities = bookService.findAllById(bookIds);

        final List<Book> collectionBookList = entity.getBooks();

        List<Book> inCollectionList = bookEntities
                .stream()
                .filter(book -> collectionBookList.contains(book))
                .collect(Collectors.toList());

        if (inCollectionList.size() > 0) {
            entity.getBooks().removeAll(inCollectionList);

            entity = bookCollectionRepository.save(entity);

            if (entity == null || entity.getId().isEmpty()) {
                throw new ServiceException(String.format("No such a book is added to collection by id: %s ", collectionId));
            }
            return new ApiResponse(true, entity.getId());
        }
        throw new DataNotFoundException(String.format("No such a book is in the collection by id: %s, nothing is deleted from collection", collectionId));

    }

    /**
     * delete book collection of authenticated user,
     * returns ApiResponse object that contains is success,
     * and if it is, record, which is the entity id
     *
     * @author umit.kas
     * @param id
     * @return ApiResponse
     *
     */
    @Override
    public ApiResponse delete(String id) throws ServiceException, NotAuthenticatedFunctionalityException {

        User authUser = userService.getAuthenticatedUserEntity();

        if (!bookCollectionRepository.existsByIdAndOwner(id, authUser)){
            throw new NotAuthenticatedFunctionalityException(String.format("Authenticated user has no permission to delete book with id: %s", id));
        }
        try {
            bookCollectionRepository.deleteById(id);
            return new ApiResponse(true, id);

        }
        catch (Exception e){
            e.printStackTrace();
            throw new ServiceException(String.format("No such a book collection record is deleted with id: %s", id));
        }
    }


    /**
     * update publicity of book collection of authenticated user,
     * returns ApiResponse object that contains is success,
     * and if it is, record, which is the entity id
     *
     * @author umit.kas
     * @param collectionId
     * @param status
     * @return ApiResponse
     *
     */
    @Override
    public ApiResponse updateCollectionPublicity(String collectionId, boolean status) throws NotAuthenticatedFunctionalityException, ServiceException {

        User authUser = userService.getAuthenticatedUserEntity();


        if (!bookCollectionRepository.existsByIdAndOwner(collectionId, authUser)){
            throw new NotAuthenticatedFunctionalityException(String.format("Authenticated user has no permission to update book collection with id: %s", collectionId));
        }

        BookCollection entity = bookCollectionRepository.findById(collectionId);

        entity.setPubliclyShared(status);

        entity = bookCollectionRepository.save(entity);

        if (entity == null || entity.getId().isEmpty()){
            throw new ServiceException("No such a book collection publicity is updated");
        }

        return new ApiResponse(true, entity.getId());

    }



    /**
     *
     * returns book collection dto by id
     *
     * @author umit.kas
     * @param id
     * @return BookCollectionDTO
     *
     */
    @Override
    public BookCollectionDTO getById(String id) throws NotAuthenticatedFunctionalityException, DataNotFoundException, ServiceException {

        User authUser = userService.getAuthenticatedUserEntity();

        BookCollection entity = bookCollectionRepository.findById(id);

        if (entity == null || entity.getId().isEmpty()) {
            throw new DataNotFoundException(String.format("No such a book collection is found to collection by id: %s ", id));
        }


        boolean isAuthUserTheOwner = bookCollectionRepository.existsByIdAndOwner(id, authUser);



        if (!entity.isPubliclyShared() && !isAuthUserTheOwner){
            throw new NotAuthenticatedFunctionalityException(String.format("Authenticated user has no permission to get book collection with id: %s", id));
        }
        else if (entity.isPubliclyShared() || isAuthUserTheOwner){
            BookCollectionDTO dto = entitiesToDTOs(Arrays.asList(entity)).get(0);
            return dto;
        }
        else {
            throw new ServiceException("No such a book collection is fetched");
        }
    }
}
