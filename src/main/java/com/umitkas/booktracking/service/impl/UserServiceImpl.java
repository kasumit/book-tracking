package com.umitkas.booktracking.service.impl;

import com.umitkas.booktracking.configuration.CustomUserDetail;
import com.umitkas.booktracking.customExceptions.ServiceException;
import com.umitkas.booktracking.customExceptions.UsernameAlreadyExistException;
import com.umitkas.booktracking.domain.User;
import com.umitkas.booktracking.dto.ApiResponse;
import com.umitkas.booktracking.repository.UserRepository;
import com.umitkas.booktracking.service.UserService;
import com.umitkas.booktracking.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserDetailsService, UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    /*
    *
    *
    * */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null){
            throw new UsernameNotFoundException(username);
        }

        return new CustomUserDetail(user);
    }

    /**
     * saves the new user
     *
     * @author umit.kas
     * @param userDTO contains username and password
     * @return ApiResponse
     *
     */

    @Override
    public ApiResponse register(UserDTO userDTO) throws UsernameAlreadyExistException, ServiceException {

        if (userRepository.existsByUsername(userDTO.getUsername())){
            throw new UsernameAlreadyExistException(String.format("%s username is already taken", userDTO.getUsername()));
        }

        User entity = new User();
        entity.setUsername(userDTO.getUsername());
        entity.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        //entity.setPassword(userDTO.getPassword());
        entity = userRepository.save(entity);

        if (entity == null || entity.getId().isEmpty()){
            throw new ServiceException("The registration is failed");
        }
        return new ApiResponse(true, entity.getUsername());
    }


    /**
     * finds the authenticated username by SecurityContextHolder, finds the user entity by username, then returns it
     * @author umit.kas
     * @return User
     *
     */
    @Override
    public User getAuthenticatedUserEntity() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return userRepository.findByUsername(authentication.getName());
    }
}
