package com.umitkas.booktracking.service.impl;

import com.umitkas.booktracking.customExceptions.DataNotFoundException;
import com.umitkas.booktracking.customExceptions.NotAuthenticatedFunctionalityException;
import com.umitkas.booktracking.customExceptions.ServiceException;
import com.umitkas.booktracking.domain.Book;
import com.umitkas.booktracking.domain.BookCollection;
import com.umitkas.booktracking.domain.User;
import com.umitkas.booktracking.dto.ApiResponse;
import com.umitkas.booktracking.dto.BookCollectionDTO;
import com.umitkas.booktracking.dto.BookDTO;
import com.umitkas.booktracking.repository.BookRepository;
import com.umitkas.booktracking.service.BookService;
import com.umitkas.booktracking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService{

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private UserService userService;


    /**
     * returns the book list of authenticated user, returns 5 object for each page
     *
     * @author umit.kas
     * @param page
     * @return List<BookDTO>
     *
     */

    @Override
    public List<BookDTO> getAuthenticatedUserBooks(int page) throws DataNotFoundException {
        User authUser = userService.getAuthenticatedUserEntity();
        List<Book> books = bookRepository.findAllByOwnerOrderByUpdatedAtDesc(authUser, new PageRequest(page, 5));

        if (books == null){
            throw new DataNotFoundException("No such a book is found for authenticated user");
        }

        // model mapper can short the code, but never used it before.
        // TODO: 14.02.2018 @umit.kas fix with model mapper

        List<BookDTO> bookDTOS = books
                .stream()
                .map(entity -> {

                    BookDTO dto = new BookDTO();
                    dto.setName(entity.getName());
                    dto.setAuthor(entity.getAuthor());
                    dto.setUpdatedAt(entity.getUpdatedAt());
                    dto.setId(entity.getId());

                    if (entity.getCollections() != null) {
                        List<BookCollectionDTO> bookCollectionDTOS = new ArrayList<>();

                        for (BookCollection collectionEntity : entity.getCollections()) {

                            BookCollectionDTO collection = new BookCollectionDTO();
                            collection.setId(collectionEntity.getId());
                            collection.setName(collectionEntity.getName());
                            collection.setPubliclyShared(collectionEntity.isPubliclyShared());
                            bookCollectionDTOS.add(collection);
                        }
                        dto.setCollections(bookCollectionDTOS);
                    }


                    return dto;

                })
                .collect(Collectors.toList());

        return bookDTOS;
    }



    /**
     * saves book for authenticated user, returns ApiResponse object that contains is success, and if it is record, which is the entity id
     *
     * @author umit.kas
     * @param dto
     * @return ApiResponse
     *
     */

    @Override
    public ApiResponse save(BookDTO dto) throws ServiceException {
        User authUser = userService.getAuthenticatedUserEntity();

        Book entity = new Book();

        entity.setName(dto.getName());
        entity.setAuthor(dto.getAuthor());
        entity.setOwner(authUser);

        entity = bookRepository.save(entity);

        if (entity == null || entity.getId().isEmpty()){
            throw new ServiceException("No such a book is saved");
        }

        return new ApiResponse(true, entity.getId());
    }


    /**
     * update book of authenticated user, returns ApiResponse object that contains is success, and if it is record, which is the entity id
     *
     * @author umit.kas
     * @param dto
     * @return ApiResponse
     *
     */
    @Override
    public ApiResponse update(BookDTO dto) throws DataNotFoundException, ServiceException, NotAuthenticatedFunctionalityException {

        User authUser = userService.getAuthenticatedUserEntity();

        String id = dto.getId();

        if (!bookRepository.existsByIdAndOwner(id, authUser)){
            throw new NotAuthenticatedFunctionalityException(String.format("Authenticated user has no permission to update book with id: %s", id));
        }

        Book entity = bookRepository.findById(id);

        if (entity == null){
            throw new DataNotFoundException(String.format("No such a book is found by id: %s", dto.getId()));
        }

        entity.setName(dto.getName());
        entity.setAuthor(dto.getAuthor());

        entity = bookRepository.save(entity);

        if (entity == null || entity.getId().isEmpty()){
            throw new ServiceException(String.format("No such a book is updated by id: %s", id));
        }

        return new ApiResponse(true, entity.getId());

    }


    /**
     * delete book of authenticated user, returns ApiResponse object that contains is success, and if it is, record, which is the entity id
     *
     * @author umit.kas
     * @param id
     * @return ApiResponse
     *
     */

    @Override
    public ApiResponse delete(String id) throws NotAuthenticatedFunctionalityException, ServiceException {

        User authUser = userService.getAuthenticatedUserEntity();

        if (!bookRepository.existsByIdAndOwner(id, authUser)){
            throw new NotAuthenticatedFunctionalityException(String.format("Authenticated user has no permission to delete book with id: %s", id));
        }
        try {
            bookRepository.deleteById(id);
            return new ApiResponse(true, id);

        }
        catch (Exception e){
            //e.printStackTrace();
            throw new ServiceException(String.format("No such a book record is deleted with id: %s", id));

        }
    }


    /**
     * NOTE: USE IT ONLY IN SERVICE LAYER
     * returns the book entities of authenticated user, used in only services, not in controller.
     *
     * @author umit.kas
     * @param bookIds
     * @return List<Book>
     *
     */

    @Override
    public List<Book> findAllById(List<String> bookIds) throws DataNotFoundException {
        User authUser = userService.getAuthenticatedUserEntity();

        List<Book> entities = bookRepository.findAllByIdInAndOwner(bookIds, authUser);

        if (entities == null || entities.size() == 0){
            throw new DataNotFoundException("No such a book record is found");

        }
        return entities;
    }
}
