package com.umitkas.booktracking.service;

import com.umitkas.booktracking.customExceptions.DataAlreadyExistException;
import com.umitkas.booktracking.customExceptions.DataNotFoundException;
import com.umitkas.booktracking.customExceptions.NotAuthenticatedFunctionalityException;
import com.umitkas.booktracking.customExceptions.ServiceException;
import com.umitkas.booktracking.domain.Book;
import com.umitkas.booktracking.domain.BookCollection;
import com.umitkas.booktracking.dto.ApiResponse;
import com.umitkas.booktracking.dto.BookCollectionDTO;
import com.umitkas.booktracking.dto.BookDTO;

import java.util.List;

public interface BookCollectionService {

    // TODO: 14.02.2018 add save update delete

    List<BookCollectionDTO> getAuthenticatedUserBookCollections(int page) throws DataNotFoundException;

    List<BookCollectionDTO> getPubliclySharedBookCollectionsByOtherUsers(int page) throws DataNotFoundException;

    List<BookCollectionDTO> getAuthenticatedUserBookCollectionsByPubliclyShared(boolean shared, int page) throws DataNotFoundException;

    ApiResponse save(BookCollectionDTO dto) throws ServiceException;

    ApiResponse update(BookCollectionDTO dto) throws NotAuthenticatedFunctionalityException, ServiceException;

    ApiResponse delete(String id) throws ServiceException, NotAuthenticatedFunctionalityException;

    ApiResponse addBooks(String collectionId, List<String> bookIds) throws NotAuthenticatedFunctionalityException, DataNotFoundException, ServiceException, DataAlreadyExistException;

    ApiResponse deleteBooks(String collectionId, List<String> bookIds) throws NotAuthenticatedFunctionalityException, DataNotFoundException, ServiceException;

    ApiResponse updateCollectionPublicity(String collectionId, boolean status) throws NotAuthenticatedFunctionalityException, ServiceException;

    BookCollectionDTO getById(String id) throws NotAuthenticatedFunctionalityException, DataNotFoundException, ServiceException;
}
