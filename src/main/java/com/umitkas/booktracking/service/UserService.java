package com.umitkas.booktracking.service;

import com.umitkas.booktracking.customExceptions.ServiceException;
import com.umitkas.booktracking.customExceptions.UsernameAlreadyExistException;
import com.umitkas.booktracking.domain.User;
import com.umitkas.booktracking.dto.ApiResponse;
import com.umitkas.booktracking.dto.UserDTO;

public interface UserService {


    ApiResponse register(UserDTO userDTO) throws UsernameAlreadyExistException, ServiceException;

    User getAuthenticatedUserEntity();
}
