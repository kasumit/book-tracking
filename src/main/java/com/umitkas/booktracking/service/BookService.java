package com.umitkas.booktracking.service;

import com.umitkas.booktracking.customExceptions.DataNotFoundException;
import com.umitkas.booktracking.customExceptions.NotAuthenticatedFunctionalityException;
import com.umitkas.booktracking.customExceptions.ServiceException;
import com.umitkas.booktracking.domain.Book;
import com.umitkas.booktracking.dto.ApiResponse;
import com.umitkas.booktracking.dto.BookDTO;

import java.util.List;

public interface BookService {

    // TODO: 14.02.2018  add save, update, delete


    List<BookDTO> getAuthenticatedUserBooks(int page) throws DataNotFoundException;

    List<Book> findAllById(List<String> bookIds) throws DataNotFoundException;

    ApiResponse save(BookDTO dto) throws ServiceException;

    ApiResponse update(BookDTO dto) throws DataNotFoundException, ServiceException, NotAuthenticatedFunctionalityException;

    ApiResponse delete(String id) throws NotAuthenticatedFunctionalityException, ServiceException;

}
