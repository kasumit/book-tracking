package com.umitkas.booktracking.domain;

import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "users")
public class User extends BaseEntity{


    private String username;

    @JsonIgnore
    private String password;

    @JsonIgnore
    private boolean enabled = true;

    @JsonIgnore
    private boolean blocked;

//    @JsonIgnore
//    @OneToMany(mappedBy = "owner")
//    private List<Book> books;

//    @JsonIgnore
//    @OneToMany(mappedBy = "owner")
//    private List<BookCollection> bookCollections;
}
