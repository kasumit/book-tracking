package com.umitkas.booktracking.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "books")
@Data
public class Book extends BaseEntity{

    private String name;

    private String author;

    //private String isbn;

    //private Date publishedAt;

    @ManyToMany(mappedBy = "books", fetch = FetchType.EAGER)
    private List<BookCollection> collections;

    @OneToOne
    @NotNull
    private User owner;
}
