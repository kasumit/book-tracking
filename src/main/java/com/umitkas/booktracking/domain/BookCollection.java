package com.umitkas.booktracking.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "book_collections")
@Data
public class BookCollection extends BaseEntity{

    private String name;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Book> books;

    @OneToOne
    @NotNull
    private User owner;

    private boolean publiclyShared;


}

