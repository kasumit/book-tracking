package com.umitkas.booktracking.customExceptions;



public class DataNotFoundException extends Exception {

    private static final long serialVersionUID = -8312225222116108742L;

    public DataNotFoundException(String s) {
        super(s);
    }

    public DataNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
