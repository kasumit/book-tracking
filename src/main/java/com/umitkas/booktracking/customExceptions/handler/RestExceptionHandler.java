package com.umitkas.booktracking.customExceptions.handler;

import com.umitkas.booktracking.customExceptions.*;
import com.umitkas.booktracking.customExceptions.messages.ApiMessage;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(value = {UsernameAlreadyExistException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public ApiMessage handleUsernameAlreadyExistException(UsernameAlreadyExistException ex){
        return new ApiMessage(HttpStatus.CONFLICT, ex.getMessage());
    }

    @ExceptionHandler(value = {RejectedParameterException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiMessage handleRejectedParameterException(RejectedParameterException ex){
        return new ApiMessage(HttpStatus.BAD_REQUEST, ex.getMessage());
    }


    @ExceptionHandler(value = {NotAuthenticatedFunctionalityException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public ApiMessage handleNotAuthenticatedFunctionalityException(NotAuthenticatedFunctionalityException ex){
        return new ApiMessage(HttpStatus.UNAUTHORIZED, ex.getMessage());
    }



    @ExceptionHandler(value = {DataNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ApiMessage handleDataNotFoundException(DataNotFoundException ex){
        return new ApiMessage(HttpStatus.NOT_FOUND, ex.getMessage());
    }


    @ExceptionHandler(value = {EmptyFieldException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiMessage handleEmptyFieldException(EmptyFieldException ex){
        return new ApiMessage(HttpStatus.BAD_REQUEST, ex.getMessage());
    }

    @ExceptionHandler(value = {ServiceException.class})
    @ResponseStatus(HttpStatus.GONE)
    @ResponseBody
    public ApiMessage handleServiceException(ServiceException ex){
        return new ApiMessage(HttpStatus.GONE, ex.getMessage());
    }

    @ExceptionHandler(value = {DataAlreadyExistException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public ApiMessage handleDataAlreadyExistException(DataAlreadyExistException ex){
        return new ApiMessage(HttpStatus.CONFLICT, ex.getMessage());
    }

}
