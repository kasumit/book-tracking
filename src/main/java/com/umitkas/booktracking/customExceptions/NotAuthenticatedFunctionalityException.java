package com.umitkas.booktracking.customExceptions;

public class NotAuthenticatedFunctionalityException extends Exception{

    public NotAuthenticatedFunctionalityException(String s) {
        super(s);
    }

    public NotAuthenticatedFunctionalityException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
