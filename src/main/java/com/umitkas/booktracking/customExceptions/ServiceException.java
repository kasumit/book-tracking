package com.umitkas.booktracking.customExceptions;

public class ServiceException extends Exception{

    public ServiceException(String s) {
        super(s);
    }
}
