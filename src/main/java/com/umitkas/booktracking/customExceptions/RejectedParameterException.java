package com.umitkas.booktracking.customExceptions;

public class RejectedParameterException extends Exception{

    public RejectedParameterException(String s) {
        super(s);
    }

    public RejectedParameterException(String s, Throwable throwable) {
        super(s, throwable);
    }


}
