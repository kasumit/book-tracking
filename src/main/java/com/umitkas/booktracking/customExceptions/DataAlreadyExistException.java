package com.umitkas.booktracking.customExceptions;

public class DataAlreadyExistException extends Exception{


    private static final long serialVersionUID = -8741773104973760481L;

    public DataAlreadyExistException(String s) {
        super(s);
    }

    public DataAlreadyExistException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
