package com.umitkas.booktracking.customExceptions;

public class UsernameAlreadyExistException extends Exception {

    private static final long serialVersionUID = -1547414878179356789L;

    public UsernameAlreadyExistException(String s) {
        super(s);
    }

}
