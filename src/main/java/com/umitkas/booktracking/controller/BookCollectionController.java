package com.umitkas.booktracking.controller;

import com.umitkas.booktracking.customExceptions.*;
import com.umitkas.booktracking.dto.ApiResponse;
import com.umitkas.booktracking.dto.BookCollectionDTO;
import com.umitkas.booktracking.service.BookCollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping(value = {"/api"})
public class BookCollectionController {


    @Autowired
    private BookCollectionService bookCollectionService;

    @GetMapping(value = {"/me/collections/{page}"})
    public List<BookCollectionDTO> getCollections(@PathVariable int page) throws RejectedParameterException, DataNotFoundException {

        if (page < 1)
            throw new RejectedParameterException("Page parameter cannot be less then 1");

        return bookCollectionService.getAuthenticatedUserBookCollections(--page);
    }

    @GetMapping(value = {"/me/private-collections/{page}"})
    public List<BookCollectionDTO> getPrivateCollections(@PathVariable int page) throws RejectedParameterException, DataNotFoundException {

        if (page < 1)
            throw new RejectedParameterException("Page parameter cannot be less then 1");

        return bookCollectionService.getAuthenticatedUserBookCollectionsByPubliclyShared(false, --page);
    }


    @GetMapping(value = {"/me/public-collections/{page}"})
    public List<BookCollectionDTO> getMyPrivateCollections(@PathVariable int page) throws RejectedParameterException, DataNotFoundException {

        if (page < 1)
            throw new RejectedParameterException("Page parameter cannot be less then 1");

        return bookCollectionService.getAuthenticatedUserBookCollectionsByPubliclyShared(true, --page);
    }

    @GetMapping(value = {"/public-collections/{page}"})
    public List<BookCollectionDTO> getPublicCollections(@PathVariable int page) throws RejectedParameterException, DataNotFoundException {

        if (page < 1)
            throw new RejectedParameterException("Page parameter cannot be less then 1");

        return bookCollectionService.getPubliclySharedBookCollectionsByOtherUsers(--page);
    }


    @PostMapping(value = {"/collection"})
    public ApiResponse save(@RequestBody BookCollectionDTO dto) throws EmptyFieldException, ServiceException {

        if (dto.getName() == null || dto.getName().isEmpty()){
            throw new EmptyFieldException("Book collection name cannot be empty");
        }

        return bookCollectionService.save(dto);
    }


    @PutMapping(value = {"/collection"})
    public ApiResponse update(@RequestBody BookCollectionDTO dto) throws EmptyFieldException, ServiceException, NotAuthenticatedFunctionalityException {

        if (dto.getId() == null || dto.getId().isEmpty()){
            throw new EmptyFieldException("Book collection id cannot be empty");
        }
        else if (dto.getName() == null || dto.getName().isEmpty()){
            throw new EmptyFieldException("Book collection name cannot be empty");
        }

        return bookCollectionService.update(dto);
    }

    @DeleteMapping(value = {"/collection/{id}"})
    public ApiResponse delete(@PathVariable String id) throws EmptyFieldException, ServiceException, NotAuthenticatedFunctionalityException {

        if (id == null || id.isEmpty()){
            throw new EmptyFieldException("Path parameter book collection id cannot be empty");
        }

        return bookCollectionService.delete(id);
    }


    @PostMapping(value = {"/collection/{collectionId}/book"})
    public ApiResponse addBook(@PathVariable String collectionId, @RequestBody List<String> bookIds) throws ServiceException, NotAuthenticatedFunctionalityException, DataNotFoundException, DataAlreadyExistException {
        return bookCollectionService.addBooks(collectionId, bookIds);
    }


    @DeleteMapping(value = {"/collection/{collectionId}/book"})
    public ApiResponse deleteBook(@PathVariable String collectionId, @RequestBody List<String> bookIds) throws ServiceException, NotAuthenticatedFunctionalityException, DataNotFoundException {
        return bookCollectionService.deleteBooks(collectionId, bookIds);
    }

    @PutMapping(value = {"/collection/{collectionId}/public"})
    public ApiResponse updatePublicityOfCollectionToPublic(@PathVariable String collectionId) throws ServiceException, NotAuthenticatedFunctionalityException {
        return bookCollectionService.updateCollectionPublicity(collectionId, true);
    }

    @PutMapping(value = {"/collection/{collectionId}/private"})
    public ApiResponse updatePublicityOfCollectionToPrivate(@PathVariable String collectionId) throws ServiceException, NotAuthenticatedFunctionalityException {
        return bookCollectionService.updateCollectionPublicity(collectionId, false);
    }

    @GetMapping(value = {"/collection/{collectionId}"})
    public BookCollectionDTO getById(@PathVariable String collectionId) throws ServiceException, NotAuthenticatedFunctionalityException, DataNotFoundException {
        return bookCollectionService.getById(collectionId);
    }

}
