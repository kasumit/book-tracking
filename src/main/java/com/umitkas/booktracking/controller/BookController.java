package com.umitkas.booktracking.controller;

import com.umitkas.booktracking.customExceptions.*;
import com.umitkas.booktracking.dto.ApiResponse;
import com.umitkas.booktracking.dto.BookDTO;
import com.umitkas.booktracking.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = {"/api"})
public class BookController {

    @Autowired
    private BookService bookService;


    @GetMapping(value = {"/me/books/{page}"})
    public List<BookDTO> getAuthenticatedUserBooks(@PathVariable int page) throws DataNotFoundException, RejectedParameterException {

        if (page < 1)
            throw new RejectedParameterException("Page parameter cannot be less then 1");

        return bookService.getAuthenticatedUserBooks(--page);
    }


    @PostMapping(value = {"/book"})
    public ApiResponse save(@RequestBody BookDTO dto) throws EmptyFieldException, ServiceException {
        if (dto.getName() == null || dto.getName().isEmpty()){
            throw new EmptyFieldException("Book name cannot be empty");
        }
        else if(dto.getAuthor() == null || dto.getAuthor().isEmpty()){
            throw new EmptyFieldException("Book author cannot be empty");
        }

        return bookService.save(dto);
    }

    @PutMapping(value = {"/book"})
    public ApiResponse update(@RequestBody BookDTO dto) throws EmptyFieldException, ServiceException, NotAuthenticatedFunctionalityException, DataNotFoundException {

        if (dto.getId() == null || dto.getId().isEmpty()){
            throw new EmptyFieldException("Book id cannot be empty");
        }
        else if (dto.getName() == null || dto.getName().isEmpty()){
            throw new EmptyFieldException("Book name cannot be empty");
        }
        else if(dto.getAuthor() == null || dto.getAuthor().isEmpty()){
            throw new EmptyFieldException("Book author cannot be empty");
        }

        return bookService.update(dto);
    }

    @DeleteMapping(value = {"/book/{id}"})
    public ApiResponse delete(@PathVariable String id) throws NotAuthenticatedFunctionalityException, ServiceException, EmptyFieldException {
        if (id == null || id.isEmpty()){
            throw new EmptyFieldException("Path parameter book id cannot be empty");
        }
        return bookService.delete(id);
    }



}
