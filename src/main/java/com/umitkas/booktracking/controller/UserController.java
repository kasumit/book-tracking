package com.umitkas.booktracking.controller;

import com.umitkas.booktracking.customExceptions.EmptyFieldException;
import com.umitkas.booktracking.customExceptions.ServiceException;
import com.umitkas.booktracking.customExceptions.UsernameAlreadyExistException;
import com.umitkas.booktracking.dto.ApiResponse;
import com.umitkas.booktracking.dto.UserDTO;
import com.umitkas.booktracking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = {"/register"})
    public ApiResponse register(@RequestBody UserDTO userDTO) throws EmptyFieldException, ServiceException, UsernameAlreadyExistException {

        if (userDTO.getUsername() == null || userDTO.getUsername().isEmpty()){
            throw new EmptyFieldException("username cannot be empty");
        }
        else if(userDTO.getPassword() == null || userDTO.getPassword().isEmpty()){
            throw new EmptyFieldException("password cannot be empty");
        }
        else if(userDTO.getPassword().length() < 6){
            throw new EmptyFieldException("password length cannot be less then 6");
        }

        return userService.register(userDTO);
    }
}
