package com.umitkas.booktracking.controller;

import com.umitkas.booktracking.service.BookCollectionService;
import com.umitkas.booktracking.service.BookService;
import com.umitkas.booktracking.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {BookCollectionController.class})
@AutoConfigureJsonTesters
public class BookCollectionControllerTest {



    @Autowired
    MockMvc mockMvc;

    @MockBean
    BookService bookService;

    @MockBean
    UserService userService;

    @MockBean
    BookCollectionService bookCollectionService;

    @Test
    @WithMockUser
    public void testSaveBookCollection() throws Exception{

        String requestPayload = JSONParser.parseJSON("{" +
                "    name: 'test book collection'" +
                "}").toString();


        MockHttpServletRequestBuilder request = post("/api/collection")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestPayload);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isOk());
        Mockito.verify(bookCollectionService).save(Mockito.any());
    }

    @Test
    @WithMockUser
    public void testSaveBookCollectionEmptyFieldException() throws Exception{

        String requestPayload = JSONParser.parseJSON("{" +
                "    name: ''" +
                "}").toString();


        MockHttpServletRequestBuilder request = post("/api/collection")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestPayload);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isBadRequest());


    }

    @Test
    @WithMockUser
    public void testSaveBookCollectionNullFieldException() throws Exception{

        String requestPayload = JSONParser.parseJSON("{" +
                "}").toString();


        MockHttpServletRequestBuilder request = post("/api/collection")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestPayload);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void testUpdateCollectionBook() throws Exception{

        String requestPayload = JSONParser.parseJSON("{" +
                "    id: 'hkj23h4823498yh291h439283h492834y'," +
                "    name: 'test book collection - update'" +
                "}").toString();


        MockHttpServletRequestBuilder request = put("/api/collection")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestPayload);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isOk());
        Mockito.verify(bookCollectionService).update(Mockito.any());

    }

    @Test
    @WithMockUser
    public void testDeleteBookCollectionOfAuthUser() throws Exception{

        String id = UUID.randomUUID().toString();

        MockHttpServletRequestBuilder request = delete(String.format("/api/collection/%s", id))
                .contentType(MediaType.APPLICATION_JSON);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isOk());
        Mockito.verify(bookCollectionService).delete(id);

    }

    @Test
    @WithMockUser
    public void testBookCollectionOfAuthUser() throws Exception{


        MockHttpServletRequestBuilder request = get("/api/me/collections/1")
                .contentType(MediaType.APPLICATION_JSON);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isOk());
        Mockito.verify(bookCollectionService).getAuthenticatedUserBookCollections(0);

    }


    @Test
    @WithMockUser
    public void testBookCollectionOfAuthUserPageException() throws Exception{



        MockHttpServletRequestBuilder request = get("/api/me/collections/0")
                .contentType(MediaType.APPLICATION_JSON);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isBadRequest());

    }


    @Test
    @WithMockUser
    public void testPublicBookCollectionsByOthers() throws Exception{



        MockHttpServletRequestBuilder request = get("/api/public-collections/1")
                .contentType(MediaType.APPLICATION_JSON);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isOk());
        Mockito.verify(bookCollectionService).getPubliclySharedBookCollectionsByOtherUsers(0);

    }

    @Test
    @WithMockUser
    public void testPrivateBookCollectionsOfAuthUser() throws Exception{



        MockHttpServletRequestBuilder request = get("/api/me/private-collections/1")
                .contentType(MediaType.APPLICATION_JSON);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isOk());
        Mockito.verify(bookCollectionService).getAuthenticatedUserBookCollectionsByPubliclyShared(false, 0);

    }

    @Test
    @WithMockUser
    public void testPublicBookCollectionsOfAuthUser() throws Exception{



        MockHttpServletRequestBuilder request = get("/api/me/public-collections/1")
                .contentType(MediaType.APPLICATION_JSON);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isOk());
        Mockito.verify(bookCollectionService).getAuthenticatedUserBookCollectionsByPubliclyShared(true, 0);

    }



    @Test
    @WithMockUser
    public void testUpdatePublicityPublicOfBookCollection() throws Exception{

        String id = UUID.randomUUID().toString();

        MockHttpServletRequestBuilder request = put(String.format("/api/collection/%s/public", id))
                .contentType(MediaType.APPLICATION_JSON);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isOk());
        Mockito.verify(bookCollectionService).updateCollectionPublicity(id, true);

    }
    @Test
    @WithMockUser
    public void testUpdatePublicityPrivateOfBookCollection() throws Exception{

        String id = UUID.randomUUID().toString();

        MockHttpServletRequestBuilder request = put(String.format("/api/collection/%s/private", id))
                .contentType(MediaType.APPLICATION_JSON);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isOk());
        Mockito.verify(bookCollectionService).updateCollectionPublicity(id, false);

    }

    @Test
    @WithMockUser
    public void testAddBookToBookCollection() throws Exception{

        List<String> ids = Arrays.asList(UUID.randomUUID().toString(), UUID.randomUUID().toString());

        String requestPayload = JSONParser.parseJSON("[" +
                    "'" + ids.get(0) + "'," +
                "'" + ids.get(1) + "'" +
                "]").toString();

        String id = UUID.randomUUID().toString();

        MockHttpServletRequestBuilder request = post(String.format("/api/collection/%s/book", id))
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestPayload);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isOk());

        Mockito.verify(bookCollectionService).addBooks(id, ids);
    }



    @Test
    @WithMockUser
    public void testDeleteBookToBookCollection() throws Exception{

        List<String> ids = Arrays.asList(UUID.randomUUID().toString(), UUID.randomUUID().toString());

        String requestPayload = JSONParser.parseJSON("[" +
                "'" + ids.get(0) + "'," +
                "'" + ids.get(1) + "'" +
                "]").toString();

        String id = UUID.randomUUID().toString();

        MockHttpServletRequestBuilder request = delete(String.format("/api/collection/%s/book", id))
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestPayload);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isOk());

        Mockito.verify(bookCollectionService).deleteBooks(id, ids);
    }

}
