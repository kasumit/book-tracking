package com.umitkas.booktracking.controller;

import com.umitkas.booktracking.service.BookService;
import com.umitkas.booktracking.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {BookController.class})
@AutoConfigureJsonTesters
public class BookControllerTest {



    @Autowired
    MockMvc mockMvc;

    @MockBean
    BookService bookService;

    @MockBean
    UserService userService;

    @Test
    @WithMockUser
    public void testSaveBook() throws Exception{

        String requestPayload = JSONParser.parseJSON("{" +
                "    name: 'test book'," +
                "    author: 'test author'" +
                "}").toString();


        MockHttpServletRequestBuilder request = post("/api/book")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestPayload);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isOk());
        Mockito.verify(bookService).save(Mockito.any());
    }

    @Test
    @WithMockUser
    public void testSaveBookEmptyFieldException() throws Exception{

        String requestPayload = JSONParser.parseJSON("{" +
                "    name: ''," +
                "    author: 'test author'" +
                "}").toString();


        MockHttpServletRequestBuilder request = post("/api/book")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestPayload);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isBadRequest());


    }

    @Test
    @WithMockUser
    public void testSaveBookNullFieldException() throws Exception{

        String requestPayload = JSONParser.parseJSON("{" +
                "    author: 'test author'" +
                "}").toString();


        MockHttpServletRequestBuilder request = post("/api/book")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestPayload);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void testUpdateBook() throws Exception{

        String requestPayload = JSONParser.parseJSON("{" +
                "    id: 'hkj23h4823498yh291h439283h492834y'," +
                "    name: 'test book'," +
                "    author: 'test author'" +
                "}").toString();


        MockHttpServletRequestBuilder request = put("/api/book")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestPayload);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isOk());
        Mockito.verify(bookService).update(Mockito.any());

    }

    @Test
    @WithMockUser
    public void testDeleteBookOfAuthUser() throws Exception{

        String id = UUID.randomUUID().toString();

        MockHttpServletRequestBuilder request = delete(String.format("/api/book/%s", id))
                .contentType(MediaType.APPLICATION_JSON);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isOk());
        Mockito.verify(bookService).delete(id);

    }

    @Test
    @WithMockUser
    public void testBookOfAuthUser() throws Exception{


        MockHttpServletRequestBuilder request = get("/api/me/books/1")
                .contentType(MediaType.APPLICATION_JSON);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isOk());
        Mockito.verify(bookService).getAuthenticatedUserBooks(0);

    }


    @Test
    @WithMockUser
    public void testBookOfAuthUserPageException() throws Exception{


        MockHttpServletRequestBuilder request = get("/api/me/books/0")
                .contentType(MediaType.APPLICATION_JSON);

        ResultActions result = mockMvc.perform(request);
        result.andExpect(status().isBadRequest());

    }

}
