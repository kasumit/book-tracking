package com.umitkas.booktracking.service;

import com.umitkas.booktracking.App;
import com.umitkas.booktracking.customExceptions.DataNotFoundException;
import com.umitkas.booktracking.customExceptions.NotAuthenticatedFunctionalityException;
import com.umitkas.booktracking.customExceptions.ServiceException;
import com.umitkas.booktracking.dto.ApiResponse;
import com.umitkas.booktracking.dto.BookDTO;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BookServiceTest extends BookServiceTestHelper{


    @Test
    public void testSaveBookForMockUser(){

        try {

            ApiResponse apiResponse = saveBookByMockUser();
            ApiResponse apiResponse2 = saveBookByMockUser();
            ApiResponse apiResponse3 = saveBookByMockUser();


            Assert.assertTrue(apiResponse.isSuccess());
            
            Assert.assertTrue(apiResponse2.isSuccess());
            
            Assert.assertTrue(apiResponse3.isSuccess());
            

        } catch (Exception e) {
            e.printStackTrace();

            Assert.fail("book save is failed");
        }
    }

    @Test
    public void testSaveBookForMockUser2(){
        try {

            ApiResponse apiResponse = saveBookByMockUser2();
            ApiResponse apiResponse2 = saveBookByMockUser2();


            Assert.assertTrue(apiResponse.isSuccess());
            Assert.assertTrue(apiResponse2.isSuccess());



        } catch (Exception e) {
            //e.printStackTrace();

            Assert.fail("book save is failed");
        }
    }


    @Test
    public void  testDeleteBookByAuthUser(){
        try {

            ApiResponse saveResponse = saveBookByMockUser();
            

            ApiResponse deleteResponse = deleteBookByMockUser(saveResponse.getRecord());
            Assert.assertTrue(deleteResponse.isSuccess());
            
            return;

        } catch (Exception e) {
            //e.printStackTrace();

            Assert.fail(String.format("service error: %b", e instanceof ServiceException));
            return;
        }


    }



    @Test
    public void  testDeleteBookByNotAuthUser(){
        try {

            ApiResponse saveResponse = saveBookByMockUser();
            
            deleteBookByMockUser2(saveResponse.getRecord());

        } catch (Exception e) {
            //e.printStackTrace();

            Assert.assertTrue(e instanceof  NotAuthenticatedFunctionalityException);
            return;

        }
        Assert.fail("book deletion is failed");

    }



    @Test
    public void testUpdateBookByAuthUser(){
        try {
            ApiResponse apiResponse = saveBookByMockUser();
            
            BookDTO dto = new BookDTO();

            dto.setId(apiResponse.getRecord());
            dto.setName("updated record");
            dto.setAuthor("updated author");

            ApiResponse response = updateBookByMockUser(dto);

            Assert.assertTrue(response.isSuccess());

            return;

        } catch (Exception e) {
            Assert.fail("book deletion is failed");

            //e.printStackTrace();

            return;

        }

    }

    @Test
    public void testUpdateBookByNotAuthUser(){

        try {

            ApiResponse apiResponse = saveBookByMockUser();

            BookDTO dto = new BookDTO();
            dto.setId(apiResponse.getRecord());

            dto.setName("updated record by mock user 2");
            dto.setAuthor("updated author");

            updateBookByMockUser2(dto);

        } catch (Exception e) {
            //e.printStackTrace();
            Assert.assertTrue(e instanceof  NotAuthenticatedFunctionalityException);
            return;

        }


        Assert.fail("book update is failed");

    }


    @Test
    public void zTestGetAuthUserBooks(){

        try {
            List<BookDTO> books = getBooksOfMockUser();
            Assert.assertEquals(mockIndex, books.size());
            return;

        } catch (Exception e) {
            //e.printStackTrace();
            Assert.fail(String.format("no such a book is found: %b", e instanceof DataNotFoundException));
        }
    }





}
