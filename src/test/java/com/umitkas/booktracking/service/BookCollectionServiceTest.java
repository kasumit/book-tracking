package com.umitkas.booktracking.service;


import com.umitkas.booktracking.App;
import com.umitkas.booktracking.customExceptions.DataNotFoundException;
import com.umitkas.booktracking.customExceptions.NotAuthenticatedFunctionalityException;
import com.umitkas.booktracking.customExceptions.ServiceException;
import com.umitkas.booktracking.domain.BookCollection;
import com.umitkas.booktracking.dto.ApiResponse;
import com.umitkas.booktracking.dto.BookCollectionDTO;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BookCollectionServiceTest extends BookCollectionTestHelper{



    @Test
    public void testSaveBookCollectionForMockUser(){


        try {
            ApiResponse apiResponse = saveBookCollectionByMockUser(false);
            ApiResponse apiResponse2 = saveBookCollectionByMockUser(false);
            ApiResponse apiResponse3 = saveBookCollectionByMockUser(false);


            Assert.assertTrue(apiResponse.isSuccess());
            Assert.assertTrue(apiResponse2.isSuccess());
            Assert.assertTrue(apiResponse3.isSuccess());


        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Collection Save is failed");
        }
    }

    @Test
    public void test01SaveBookCollectionForMockUser2(){


        try {
            ApiResponse apiResponse = saveBookCollectionByMockUser2(false);
            ApiResponse apiResponse2 = saveBookCollectionByMockUser2(false);

            Assert.assertTrue(apiResponse.isSuccess());
            Assert.assertTrue(apiResponse2.isSuccess());


        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Collection Save is failed");
        }
    }


    @Test
    public void test02DeleteBookCollectionByAuthUser(){

        try {

            ApiResponse saveResponse = saveBookCollectionByMockUser(false);

            ApiResponse deleteResponse = deleteBookCollectionByMockUser(saveResponse.getRecord());

            Assert.assertTrue(deleteResponse.isSuccess());
            return;

        } catch (Exception e) {
            e.printStackTrace();

            Assert.fail(String.format("service error: %b", e instanceof ServiceException));
            return;

        }

    }


    @Test
    public void  test03DeleteBookCollectionByNotAuthUser(){

        try {

            ApiResponse saveResponse = saveBookCollectionByMockUser(false);

            ApiResponse deleteResponse = deleteBookCollectionByMockUser2(saveResponse.getRecord());

        } catch (Exception e) {
            e.printStackTrace();

            Assert.assertTrue(e instanceof NotAuthenticatedFunctionalityException);
            return;

        }

    Assert.fail("deletion is failed");





    }

    @Test
    public void test04UpdateBookCollectionByAuthUser(){

        try {
            ApiResponse saveResponse = saveBookCollectionByMockUser(false);

            BookCollectionDTO dto = new BookCollectionDTO();
            dto.setId(saveResponse.getRecord());
            dto.setName("collection - updated");
            dto.setPubliclyShared(true);

            ApiResponse updateResponse = updateBookCollectionByMockUser(dto);

            Assert.assertTrue(updateResponse.isSuccess());
            Assert.assertEquals(saveResponse.getRecord(), updateResponse.getRecord());
            return;

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(String.format("service error: ", e instanceof ServiceException));
            return;
        }


    }


    @Test
    public void test05UpdateBookCollectionByNotAuthUser(){

        try {
            ApiResponse saveResponse = saveBookCollectionByMockUser(false);

            BookCollectionDTO dto = new BookCollectionDTO();
            dto.setId(saveResponse.getRecord());
            dto.setName("collection - updated");
            dto.setPubliclyShared(true);

            ApiResponse updateResponse = updateBookCollectionByMockUser2(dto);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.assertTrue(e instanceof  NotAuthenticatedFunctionalityException);
            return;
        }
    }




    @Test
    public void test06AddBookToBookCollectionByAuthUser(){

        try {

            ApiResponse saveBookResponse = saveBookByMockUser();

            Assert.assertTrue(saveBookResponse.isSuccess());

            ApiResponse saveBookResponse2 = saveBookByMockUser();

            Assert.assertTrue(saveBookResponse2.isSuccess());

            List<String> bookIds = Arrays.asList(saveBookResponse.getRecord(), saveBookResponse2.getRecord());

            ApiResponse saveBookCollectionResponse = saveBookCollectionByMockUser(false);

            Assert.assertTrue(saveBookCollectionResponse.isSuccess());

            ApiResponse addBookToCollectionResponse = addBookToBookCollectionByMockUser(saveBookCollectionResponse.getRecord(), bookIds);

            Assert.assertTrue(addBookToCollectionResponse.isSuccess());

            BookCollectionDTO dto = bookCollectionService.getById(saveBookCollectionResponse.getRecord());

            Assert.assertEquals(bookIds.size(), dto.getBooks().size());
            return;

        } catch (Exception e) {
            e.printStackTrace();
        }


    }



    @Test
    public void test07AddTheBookOfMockUser2ToTheBookCollectionOfMockUserByMockUser(){


        try {

            ApiResponse saveBookResponse = saveBookByMockUser2();

            Assert.assertTrue(saveBookResponse.isSuccess());

            ApiResponse saveBookResponse2 = saveBookByMockUser2();

            Assert.assertTrue(saveBookResponse2.isSuccess());

            List<String> bookIds = Arrays.asList(saveBookResponse.getRecord(), saveBookResponse2.getRecord());

            ApiResponse saveBookCollectionResponse = saveBookCollectionByMockUser(false);

            Assert.assertTrue(saveBookCollectionResponse.isSuccess());

            // will throw DataNotFoundException because will not find books by authenticated user
            addBookToBookCollectionByMockUser(saveBookCollectionResponse.getRecord(), bookIds);



        } catch (Exception e) {
            e.printStackTrace();

            Assert.assertTrue(e instanceof DataNotFoundException);

            return;

        }



        Assert.fail("exception is not cached");
    }


    @Test
    public void test07AddTheBookOfMockUser2ToTheBookCollectionOfMockUserByMockUser2(){



        try {

            ApiResponse saveBookResponse = saveBookByMockUser2();

            Assert.assertTrue(saveBookResponse.isSuccess());

            ApiResponse saveBookResponse2 = saveBookByMockUser2();

            Assert.assertTrue(saveBookResponse2.isSuccess());

            List<String> bookIds = Arrays.asList(saveBookResponse.getRecord(), saveBookResponse2.getRecord());

            ApiResponse saveBookCollectionResponse = saveBookCollectionByMockUser(false);

            Assert.assertTrue(saveBookCollectionResponse.isSuccess());

            addBookToBookCollectionByMockUser2(saveBookCollectionResponse.getRecord(), bookIds);


        } catch (Exception e) {
            e.printStackTrace();

            Assert.assertTrue(e instanceof NotAuthenticatedFunctionalityException);

            return;

        }
        Assert.fail("exception is not cached");

    }



    @Test
    public void test09BookCollectionOfAuthUser() {

        try {

           List<BookCollectionDTO> collections = getBooksCollectionOfMockUser();

           Assert.assertEquals(mockCollectionIndex, collections.size());
           return;

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("failed");
        }

    }


    @Test
    public void test10BookCollectionOfAuthUserByPublicity() {

        try {
            List<BookCollectionDTO> collections = getBooksCollectionOfMockUser();

            for (BookCollectionDTO collection : collections){

                updatePublicityOfCollectionByMockUser(collection.getId(), true);
            }

            List<BookCollectionDTO> publicCollections = getBooksCollectionOfMockUserByPublicity(true);

            Assert.assertEquals(collections.size(), publicCollections.size());

            publicCollections = getBooksCollectionOfMockUserByPublicity(false);

            Assert.assertEquals(0, publicCollections.size());


            return;
        }
        catch (Exception e) {
            //e.printStackTrace();
            Assert.fail("failed");
        }

    }


    @Test
    public void test12UpdatePublicityOfCollectionByAuthUser() {

        try {
            ApiResponse saveResponse = saveBookCollectionByMockUser(false);

            ApiResponse updateResponse = updatePublicityOfCollectionByMockUser(saveResponse.getRecord(), true);

            Assert.assertTrue(updateResponse.isSuccess());
            return;
        } catch (Exception e) {
            //e.printStackTrace();
            Assert.fail("failed");
        }

    }

    @Test
    public void test13UpdatePublicityOfCollectionByNotAuthUser() {


        try {
            ApiResponse saveResponse = saveBookCollectionByMockUser(false);

           updatePublicityOfCollectionByMockUser2(saveResponse.getRecord(), true);

        } catch (Exception e) {
            Assert.assertTrue(e instanceof NotAuthenticatedFunctionalityException);
            return;
        }
        Assert.fail("Not Catched");
    }


    @Test
    public void test14DeleteBookFromCollectionByNotAuthUser() {


        try {

            ApiResponse saveResponse = saveBookCollectionByMockUser(true);

            deleteBookCollectionByMockUser2(saveResponse.getRecord());

        } catch (Exception e) {
            Assert.assertTrue(e instanceof NotAuthenticatedFunctionalityException);
            return;
        }
        Assert.fail("catch is not worked");
    }

    @Test
    public void test15DeleteBookFromCollectionByAuthUser() {



        try {

            ApiResponse saveResponse = saveBookCollectionByMockUser(true);

            deleteBookCollectionByMockUser(saveResponse.getRecord());
            return;

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("failed");

        }
    }


    @Test
    public void test16BookCollectionByIdByAuthUser() {

        try {

            ApiResponse saveResponse = saveBookCollectionByMockUser(true);

            BookCollectionDTO collection = getBookCollectionByIdByMockUser(saveResponse.getRecord());
            Assert.assertEquals(saveResponse.getRecord(), collection.getId());
            return;

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("failed");

        }
    }


    @Test
    public void test18BookCollectionByIdByPublicityFalseByNotAuthUser() {

        try {

            ApiResponse saveResponse = saveBookCollectionByMockUser(false);

            getBookCollectionByIdByMockUser2(saveResponse.getRecord());

        } catch (Exception e) {
            Assert.assertTrue(e instanceof NotAuthenticatedFunctionalityException);
            return;
        }
        Assert.fail("catch is not worked");
    }


    @Test
    public void test18BookCollectionByIdByPublicityTrueByNotAuthUser() {

        try {

            ApiResponse saveResponse = saveBookCollectionByMockUser(true);

            BookCollectionDTO collection = getBookCollectionByIdByMockUser2(saveResponse.getRecord());
            Assert.assertEquals(saveResponse.getRecord(), collection.getId());
            return;

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("failed");

        }
    }

}
