package com.umitkas.booktracking.service;

import com.umitkas.booktracking.customExceptions.DataAlreadyExistException;
import com.umitkas.booktracking.customExceptions.DataNotFoundException;
import com.umitkas.booktracking.customExceptions.NotAuthenticatedFunctionalityException;
import com.umitkas.booktracking.customExceptions.ServiceException;
import com.umitkas.booktracking.dto.ApiResponse;
import com.umitkas.booktracking.dto.BookCollectionDTO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class BookCollectionTestHelper extends BookServiceTestHelper {


    private static int index;

    protected static int mockCollectionIndex;

    @Autowired
    protected BookCollectionService bookCollectionService;



    private ApiResponse saveBookCollection(String text, boolean publicity) throws ServiceException {
        BookCollectionDTO dto1 = new BookCollectionDTO();

        index++;
        dto1.setName(String.format("book Collection - %d by %s", index, text));
        dto1.setPubliclyShared(publicity);
        return bookCollectionService.save(dto1);
    }

    public ApiResponse saveBookCollectionByMockUser(boolean publicity) throws ServiceException {
        updateSecurityContext("mock.user", "password");
        ApiResponse resp = saveBookCollection("mock.user", publicity);
        mockCollectionIndex++;
        return resp;
    }

    public ApiResponse saveBookCollectionByMockUser2(boolean publicity) throws ServiceException {
        updateSecurityContext("mock.user2", "password");
        return saveBookCollection("mock.user2", publicity);
    }

    public List<BookCollectionDTO> getBooksCollectionOfMockUser() throws DataNotFoundException {
        updateSecurityContext("mock.user", "password");
        List<BookCollectionDTO> collections = new ArrayList<>();
        int page = 0;
        while (true){

            List<BookCollectionDTO> bookCollectionDTOS = bookCollectionService.getAuthenticatedUserBookCollections(page);
            if (bookCollectionDTOS.size() == 0){
                break;
            }
            collections.addAll(bookCollectionDTOS);
            page++;

        }
        return collections;

    }

    public ApiResponse deleteBookCollectionByMockUser(String collectionId) throws ServiceException, NotAuthenticatedFunctionalityException {
        updateSecurityContext("mock.user", "password");
        ApiResponse resp = bookCollectionService.delete(collectionId);
        mockCollectionIndex--;
        return resp;
    }

    public ApiResponse deleteBookCollectionByMockUser2(String bookId) throws ServiceException, NotAuthenticatedFunctionalityException {
        updateSecurityContext("mock.user2", "password");
        return bookCollectionService.delete(bookId);
    }

    public ApiResponse updateBookCollectionByMockUser(BookCollectionDTO dto) throws ServiceException, NotAuthenticatedFunctionalityException, DataNotFoundException {
        updateSecurityContext("mock.user", "password");
        return bookCollectionService.update(dto);
    }

    public ApiResponse updateBookCollectionByMockUser2(BookCollectionDTO dto) throws ServiceException, NotAuthenticatedFunctionalityException, DataNotFoundException {
        updateSecurityContext("mock.user2", "password");
        return bookCollectionService.update(dto);
    }

    public ApiResponse addBookToBookCollectionByMockUser(String collectionId, List<String> bookIds) throws DataNotFoundException, NotAuthenticatedFunctionalityException, ServiceException, DataAlreadyExistException {
        updateSecurityContext("mock.user", "password");
        return bookCollectionService.addBooks(collectionId, bookIds);
    }

    public ApiResponse addBookToBookCollectionByMockUser2(String collectionId, List<String> bookIds) throws DataNotFoundException, NotAuthenticatedFunctionalityException, ServiceException, DataAlreadyExistException {
        updateSecurityContext("mock.user2", "password");
        return bookCollectionService.addBooks(collectionId, bookIds);
    }

    public ApiResponse updatePublicityOfCollectionByMockUser(String collectionId, boolean publicity) throws NotAuthenticatedFunctionalityException, ServiceException {
        updateSecurityContext("mock.user", "password");
        return bookCollectionService.updateCollectionPublicity(collectionId, publicity);
    }

    public ApiResponse updatePublicityOfCollectionByMockUser2(String collectionId, boolean publicity) throws NotAuthenticatedFunctionalityException, ServiceException {
        updateSecurityContext("mock.user2", "password");
        return bookCollectionService.updateCollectionPublicity(collectionId, publicity);
    }

    public List<BookCollectionDTO> getBooksCollectionOfMockUserByPublicity(boolean publicity) throws DataNotFoundException {
        updateSecurityContext("mock.user", "password");
        List<BookCollectionDTO> collections = new ArrayList<>();
        int page = 0;
        while (true){

            List<BookCollectionDTO> bookCollectionDTOS = bookCollectionService.getAuthenticatedUserBookCollectionsByPubliclyShared(publicity, page);
            if (bookCollectionDTOS.size() == 0){
                break;
            }
            collections.addAll(bookCollectionDTOS);
            page++;

        }
        return collections;

    }


    public BookCollectionDTO getBookCollectionByIdByMockUser(String collectionId) throws NotAuthenticatedFunctionalityException, DataNotFoundException, ServiceException {
        updateSecurityContext("mock.user", "password");
        return bookCollectionService.getById(collectionId);
    }

    public BookCollectionDTO getBookCollectionByIdByMockUser2(String collectionId) throws NotAuthenticatedFunctionalityException, DataNotFoundException, ServiceException {
        updateSecurityContext("mock.user2", "password");
        return bookCollectionService.getById(collectionId);
    }

}
