package com.umitkas.booktracking.service;

import com.umitkas.booktracking.App;
import com.umitkas.booktracking.customExceptions.UsernameAlreadyExistException;
import com.umitkas.booktracking.dto.ApiResponse;
import com.umitkas.booktracking.dto.UserDTO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class UserServiceTest {


    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;



    UserDTO user(){
        UserDTO dto = new UserDTO();
        dto.setUsername("test.user");
        dto.setPassword("mock.password");
        return dto;
    }

    UserDTO user2(){
        UserDTO dto = new UserDTO();
        dto.setUsername("test.user2");
        dto.setPassword("mock.password");
        return dto;
    }

    @Test
    public void testRegisterSuccess(){

        try {
            ApiResponse response = userService.register(user());
            assertTrue(response.isSuccess());
        }
        catch (Exception e) {
            fail("Not registered");
        }
    }



    @Test
    public void testRegisterUsernameConflict(){

        try {
            userService.register(user2());
            userService.register(user2());

        }
        catch (Exception e) {
            assertTrue(e instanceof UsernameAlreadyExistException);
        }
    }


}
