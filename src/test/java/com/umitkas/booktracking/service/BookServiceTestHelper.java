package com.umitkas.booktracking.service;

import com.umitkas.booktracking.customExceptions.DataNotFoundException;
import com.umitkas.booktracking.customExceptions.NotAuthenticatedFunctionalityException;
import com.umitkas.booktracking.customExceptions.ServiceException;
import com.umitkas.booktracking.dto.ApiResponse;
import com.umitkas.booktracking.dto.BookDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;


public class BookServiceTestHelper {

    @Autowired
    private BookService bookService;

    private static int index;

    protected static int mockIndex;



    protected void updateSecurityContext(String username, String password){
        SecurityContextHolder.clearContext();
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(username, password));
    }


    private ApiResponse saveBook(String text) throws ServiceException {
        BookDTO dto1 = new BookDTO();

        index++;
        dto1.setName(String.format("book - %d by %s", index, text));
        dto1.setAuthor("test author " + index);
        return bookService.save(dto1);
    }


    public ApiResponse saveBookByMockUser() throws ServiceException {
        updateSecurityContext("mock.user", "password");
        ApiResponse resp =  saveBook("mock.user");
        mockIndex++;
        return resp;
    }

    public ApiResponse saveBookByMockUser2() throws ServiceException {
        updateSecurityContext("mock.user2", "password");
        return saveBook("mock.user2");
    }



    public List<BookDTO> getBooksOfMockUser() throws DataNotFoundException {
        updateSecurityContext("mock.user", "password");
        List<BookDTO> books = new ArrayList<>();
        int page = 0;
        while (true){

            List<BookDTO> bookDTOS = bookService.getAuthenticatedUserBooks(page);
            if (bookDTOS.size() == 0){
                break;
            }
            books.addAll(bookDTOS);
            page++;

        }
        return books;

    }


    public List<BookDTO> getBooksOfMockUser2() throws DataNotFoundException {
        updateSecurityContext("mock.user2", "password");
        return bookService.getAuthenticatedUserBooks(0);
    }


    public ApiResponse deleteBookByMockUser(String bookId) throws ServiceException, NotAuthenticatedFunctionalityException {
        updateSecurityContext("mock.user", "password");
        ApiResponse resp = bookService.delete(bookId);
        mockIndex--;
        return resp;
    }


    public ApiResponse deleteBookByMockUser2(String bookId) throws ServiceException, NotAuthenticatedFunctionalityException {
        updateSecurityContext("mock.user2", "password");
        return bookService.delete(bookId);
    }



    public ApiResponse updateBookByMockUser(BookDTO dto) throws ServiceException, NotAuthenticatedFunctionalityException, DataNotFoundException {
        updateSecurityContext("mock.user", "password");
        return bookService.update(dto);
    }


    public ApiResponse updateBookByMockUser2(BookDTO dto) throws ServiceException, NotAuthenticatedFunctionalityException, DataNotFoundException {
        updateSecurityContext("mock.user2", "password");
        return bookService.update(dto);
    }

}
